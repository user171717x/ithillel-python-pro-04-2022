import random
import requests
from flask import Flask, render_template, request
from faker import Faker
from string import ascii_lowercase, digits

REQUIREMENTS_PATH = '../../requirements/requirements.txt'
GENERATOR_STR = f"{ascii_lowercase}{digits}"
EMAIL_MIN_LEN = 5
EMAIL_MAX_LEN = 15
DOMAIN_MIN_LEN = 3
DOMAIN_MAX_LEN = 8
EXTENSION_LEN = 3

app = Flask(__name__)
fake = Faker()  # fake = Faker('ru_RU') - with russians names lastnames comes first very often (unusable)


def create_email():
    email_len = random.randint(EMAIL_MIN_LEN, EMAIL_MAX_LEN)
    domain_len = random.randint(DOMAIN_MIN_LEN, DOMAIN_MAX_LEN)
    email = ''.join(random.choice(GENERATOR_STR) for _ in range(email_len))
    domain = ''.join(random.choice(ascii_lowercase) for _ in range(domain_len))
    extension = ''.join(random.choice(ascii_lowercase) for _ in range(EXTENSION_LEN))
    return f"{email}@{domain}.{extension}"


def create_name():
    return str(fake.name()).split()[0]


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/requirements/')
def requirements():
    with open(REQUIREMENTS_PATH, 'r') as file:
        context = [item.split('==') for item in file.read().split()]
    return render_template('requirements.html', usage=context)


@app.route('/generate-users/')
def generate_users():
    amount = request.values.get('amount')
    if amount:
        context = [f"{create_name()} {create_email()}" for _ in range(int(amount))]
    else:
        context = list()
    return render_template('generate-users.html', items=context)


@app.route('/space/')
def space():
    try:
        data = requests.get('http://api.open-notify.org/astros.json')
        astros = len(data.json()["people"])
    except requests.exceptions.RequestException:
        astros = 0
    return render_template('space.html', astros=astros)


if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000, debug=True)
